export const environment = {
  production: true,
  getVendors:'http://api.tyrewala.in/vendors/all',
  getBookings:'http://api.tyrewala.in/booking',
  getCustomers:'http://api.tyrewala.in/otp',
  insertVendor:'http://api.tyrewala.in/vendors',
  updateVendor:'http://api.tyrewala.in/vendors/',
  deleteVendor:'http://api.tyrewala.in/vendors/',
  login:'http://api.tyrewala.in/admin/adminlogin',
  totalCustomers:'http://api.tyrewala.in/booking/totalCustomers',
  totalBookings:'http://api.tyrewala.in/booking/totalBooking',
  totalSales:'http://api.tyrewala.in/booking/totalSales',
  totalComission:'http://api.tyrewala.in/booking/totalComission'
};
