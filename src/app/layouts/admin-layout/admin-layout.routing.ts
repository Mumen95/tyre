import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { VendorsComponent } from 'app/pages/vendors/venders.component';
import { CustomersComponent } from 'app/pages/customers/customers.component';
import { BookingsComponent } from 'app/pages/bookings/bookings.component';
import { AuthGuardService } from 'app/services/guards/auth-guard.service';
import { AdminLayoutComponent } from './admin-layout.component';

export const AdminLayoutRoutes: Routes = [
    {
        path:'',
        component:AdminLayoutComponent,
        children:[
            { path: 'vendors',        component: VendorsComponent,canActivate:[AuthGuardService] },
            { path: 'customers',        component: CustomersComponent,canActivate:[AuthGuardService] },
            { path: 'bookings',        component: BookingsComponent,canActivate:[AuthGuardService] },
            { path: 'dashboard',        component:DashboardComponent,canActivate:[AuthGuardService] },
            { path: '',        redirectTo:'dashboard' }
        ]
    }
];
