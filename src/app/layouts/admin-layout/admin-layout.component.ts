import { Component, OnInit, ViewChild } from '@angular/core';
import { SidebarComponent } from 'app/sidebar/sidebar.component';
import { GeneralService } from 'app/services/general.service';
import { HTTPStatus } from 'app/loading/loading-interceptor.service';


@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {


  @ViewChild ('sidebar',{static:true}) sidebar;

  constructor(private generalService:GeneralService,
    private httpStatus: HTTPStatus){}

  public shouldView=true;
  HTTPActivity: boolean;

  ngOnInit() {
    this.httpStatus.getHttpStatus().subscribe((status: boolean) => {
      console.log(status);
      this.HTTPActivity = status
    });
    this.generalService.setSidebar(this);
   }
}
