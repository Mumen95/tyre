import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { VendorService } from 'app/services/vendor.service';
import { ToastrService } from 'ngx-toastr';
import { GeneralService } from 'app/services/general.service';
import { MatDialogRef } from '@angular/material/dialog';
import * as _ from 'lodash';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.scss']
})
export class VendorComponent implements OnInit {

  imageError: string;
  isImageSaved: boolean;
  imageBinary:any;
  cardImageBase64: string;

  submitted=false;

  form: FormGroup = new FormGroup({
    shop_name: new FormControl('', [Validators.required]),
    phone_number: new FormControl('', [Validators.required]),
    status: new FormControl('1', [Validators.required]),
    device_token: new FormControl(''),
    place: new FormControl('', [Validators.required]),
    pincode: new FormControl('', [Validators.required]),
    shop_description: new FormControl('', [Validators.required]),
    lat: new FormControl(0, [Validators.required]),
    long: new FormControl(0, [Validators.required]),
    type: new FormControl('point', [Validators.required]),
    car_tube: new FormControl('', [Validators.required]),
    bike_tube: new FormControl('', [Validators.required]),
    bus_tube: new FormControl('', [Validators.required]),
    car_tubeless: new FormControl('', [Validators.required]),
    bike_tubeless: new FormControl('', [Validators.required]),
    bus_tubeless: new FormControl('', [Validators.required]),
    car_rft: new FormControl('', [Validators.required]),
    bike_rft: new FormControl('', [Validators.required]),
    bus_rft: new FormControl('', [Validators.required]),
    // vendorImage: new FormControl('',[Validators.required])
  });

  constructor(private vendorService:VendorService,
    private generalService: GeneralService,
    public dialogRef: MatDialogRef<VendorComponent>,) { }

  ngOnInit() {
  }

  submit() {
    this.submitted=true;
    if (this.form.valid){
      let form = this.fixForm();
      this.vendorService.insertVendor(form).subscribe((res:any)=>{
        if (res.success){
          this.generalService.showNotification('top','center',res.message,2);
          this.dialogRef.close();
        }else{
          this.generalService.showNotification('top','center',res.message,4);
        }
      });
    }
  }

  fixForm() {
    let form: any = {};

    form.shop_name = this.form.controls['shop_name'].value;
    form.phone_number = this.form.controls['phone_number'].value;
    form.device_token = this.form.controls['device_token'].value;
    form.status = this.form.controls['status'].value;
    form.place = this.form.controls['place'].value;
    form.pincode = this.form.controls['pincode'].value;
    form.shop_description = this.form.controls['shop_description'].value;
    form.location = {
      type: this.form.controls['type'].value,
      coordinates: [this.form.controls['lat'].value, this.form.controls['long'].value]
    };
    form.price = [
      {
      car: {
        tube: this.form.controls['car_tube'].value,
        tubeless: this.form.controls['car_tubeless'].value,
        rft: this.form.controls['car_rft'].value,
      }
    },
    {
      bike: {
        tube: this.form.controls['bike_tube'].value,
        tubeless: this.form.controls['bike_tubeless'].value,
        rft: this.form.controls['bike_rft'].value,
      }
    },
    {
      bus: {
        tube: this.form.controls['bus_tube'].value,
        tubeless: this.form.controls['bus_tubeless'].value,
        rft: this.form.controls['bus_rft'].value,
      }
    },  
  ];
  form.vendorImage = this.cardImageBase64;

  return form;

  // let formData = new FormData();

  // formData.append('shop_name',this.form.controls['shop_name'].value);
  // formData.append('phone_number',this.form.controls['phone_number'].value);
  // formData.append('device_token',this.form.controls['device_token'].value);
  // formData.append('status',this.form.controls['status'].value);
  // formData.append('place',this.form.controls['place'].value);
  // formData.append('pincode',this.form.controls['place'].value);
  // formData.append('shop_description',this.form.controls['place'].value);
  // formData.append('location',JSON.stringify(form.location));
  // formData.append('price',JSON.stringify(form.price));
  // formData.append('vendorImage',this.imageBinary);
  // return formData;
  }

  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.imageBinary = fileInput.target.files[0];
        // Size Filter Bytes
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
            this.imageError =
                'Maximum size allowed is ' + max_size / 1000 + 'Mb';

            return false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
            this.imageError = 'Only Images are allowed ( JPG | PNG )';
            return false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
            const image = new Image();
            image.src = e.target.result;
            image.onload = rs => {
                const img_height = rs.currentTarget['height'];
                const img_width = rs.currentTarget['width'];

                console.log(img_height, img_width);


                if (img_height > max_height && img_width > max_width) {
                    this.imageError =
                        'Maximum dimentions allowed ' +
                        max_height +
                        '*' +
                        max_width +
                        'px';
                    return false;
                } else {
                    const imgBase64Path = e.target.result;
                    this.cardImageBase64 = imgBase64Path;
                    this.isImageSaved = true;
                    // this.previewImagePath = imgBase64Path;

                    console.log(this.cardImageBase64);
                }
            };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
    }
}

removeImage() {
    this.cardImageBase64 = null;
    this.isImageSaved = false;
    this.form.controls.vendorImage.reset();
}
}
