import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './pages/login/login.component';

export const AppRoutes: Routes = [
  {
    path: 'admin',
    loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
},
  {
    path:'login',
    component:LoginComponent
  },
  {
    path: '',
    redirectTo: 'admin',
    pathMatch: 'full',
  },
  // {
  //   path: '**',
  //   redirectTo: 'dashboard'
  // }
]
